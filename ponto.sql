
create database if not exists ponto;
use ponto;

DROP TABLE IF EXISTS `acessos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `acessos` (
  `id_acesso` bigint(20) NOT NULL AUTO_INCREMENT,
  `data_acesso` datetime DEFAULT NULL,
  `entrada` varchar(20) DEFAULT NULL,
  `saida` varchar(20) DEFAULT NULL,
  `id_usr` bigint(20) DEFAULT NULL,
  `numr_ip` varchar(200) DEFAULT NULL,
  `visao` bigint(20) NOT NULL DEFAULT '0',
  `ho` bigint(20) DEFAULT NULL,
  `pma` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_acesso`),
  KEY `id_acesso` (`id_acesso`)
) ENGINE=InnoDB AUTO_INCREMENT=163570 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `dadosconexao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `dadosconexao` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `dados` varchar(255) NOT NULL,
  `data_inclusao` datetime DEFAULT NULL,
  `dados_adicionais` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `depto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `depto` (
  `id_depto` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_superintendencia` bigint(20) DEFAULT NULL,
  `depto` varchar(200) DEFAULT NULL,
  `sigla` varchar(30) DEFAULT NULL,
  `ativo` bigint(20) DEFAULT NULL,
  `ramal` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_depto`),
  KEY `id_depto` (`id_depto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `ips_autorizados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `ips_autorizados` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(50) DEFAULT NULL,
  `local` varchar(255) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `ocorrencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `ocorrencias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `situacao` int(11) unsigned DEFAULT NULL,
  `id_usr` double DEFAULT NULL,
  `data_inclusao` datetime DEFAULT NULL,
  `mensagem` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descricao` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `etapa` int(11) unsigned DEFAULT NULL,
  `horaEsperada` int(11) DEFAULT NULL,
  `horaRegistrada` int(11) DEFAULT NULL,
  `emailEnviado` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_enviado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25334 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `p_anexos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `p_anexos` (
  `id_anexo` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_usr` bigint(20) DEFAULT NULL,
  `id_justificativa` bigint(20) DEFAULT NULL,
  `data_anexo` datetime DEFAULT NULL,
  `nivel_just` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_anexo`)
) ENGINE=InnoDB AUTO_INCREMENT=4998 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `p_eventos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `p_eventos` (
  `id_evento` bigint(20) NOT NULL AUTO_INCREMENT,
  `data_evento` datetime DEFAULT NULL,
  `descricao` varchar(5000) DEFAULT NULL,
  `id_depto` bigint(20) DEFAULT NULL,
  `id_usr` bigint(20) DEFAULT NULL,
  `id_tipo_justificativa` bigint(20) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `numr_ip` varchar(200) DEFAULT NULL,
  `id_pai` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_evento`),
  KEY `id_usr` (`id_usr`),
  KEY `data_evento` (`data_evento`)
) ENGINE=InnoDB AUTO_INCREMENT=40000 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `p_favoritos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `p_favoritos` (
  `id_favorito` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_gerencia_dono` bigint(20) DEFAULT NULL,
  `titulo_favorito` varchar(100) DEFAULT NULL,
  `id_trabalho` bigint(20) DEFAULT NULL,
  `data_favorito` datetime DEFAULT NULL,
  `instrucao_sql` varchar(3000) DEFAULT NULL,
  `instrucao_sql_total` varchar(3000) DEFAULT NULL,
  PRIMARY KEY (`id_favorito`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `p_favoritos` WRITE;
/*!40000 ALTER TABLE `p_favoritos` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_favoritos` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `p_favoritos_frame`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `p_favoritos_frame` (
  `id_favoritos_frame` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_gerencia_dono` bigint(20) DEFAULT NULL,
  `id_gerencia_favorito` bigint(20) DEFAULT NULL,
  `id_trabalho` bigint(20) DEFAULT NULL,
  `id_favorito` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_favoritos_frame`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


LOCK TABLES `p_favoritos_frame` WRITE;
/*!40000 ALTER TABLE `p_favoritos_frame` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_favoritos_frame` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `p_grade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `p_grade` (
  `id_grade` bigint(20) NOT NULL AUTO_INCREMENT,
  `nome_grade` varchar(200) DEFAULT NULL,
  `tipo_grade` bigint(20) DEFAULT NULL,
  `data_grade` datetime DEFAULT NULL,
  `id_usr_resp_grade` bigint(20) DEFAULT NULL,
  `entrada_1` varchar(5) DEFAULT NULL,
  `saida_1` varchar(5) DEFAULT NULL,
  `entrada_2` varchar(5) DEFAULT NULL,
  `saida_2` varchar(5) DEFAULT NULL,
  `ativo` bigint(20) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_grade`),
  UNIQUE KEY `idx_horario` (`entrada_1`,`saida_1`,`entrada_2`,`saida_2`,`ativo`),
  KEY `id_usr_resp_grade` (`id_usr_resp_grade`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `p_grade` WRITE;
/*!40000 ALTER TABLE `p_grade` DISABLE KEYS */;

/*!40000 ALTER TABLE `p_grade` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `p_historico_justificativa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `p_historico_justificativa` (
  `id_justificativa` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_usr` bigint(20) DEFAULT NULL,
  `id_tipo_justificativa` bigint(20) DEFAULT NULL,
  `data_justificativa` datetime DEFAULT NULL,
  `observacoes` varchar(4000) DEFAULT NULL,
  `observacoes_chefe` varchar(4000) DEFAULT NULL,
  `data_cadastro_just` datetime DEFAULT NULL,
  `abonado` bigint(20) DEFAULT NULL,
  `id_chefe` bigint(20) DEFAULT NULL,
  `data_abono` datetime DEFAULT NULL,
  `qtd_horas` varchar(5) DEFAULT NULL,
  `protocolo` varchar(100) DEFAULT NULL,
  `anexo` bigint(20) NOT NULL DEFAULT '0',
  `nome_arquivo` varchar(100) DEFAULT NULL,
  `extensao` varchar(10) DEFAULT NULL,
  `who_r` bigint(20) DEFAULT NULL,
  `ordem_justificativa` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_justificativa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `p_historico_justificativa` WRITE;
/*!40000 ALTER TABLE `p_historico_justificativa` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_historico_justificativa` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `p_justificativa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `p_justificativa` (
  `id_justificativa` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_usr` bigint(20) DEFAULT NULL,
  `id_tipo_justificativa` bigint(20) DEFAULT NULL,
  `data_justificativa` datetime DEFAULT NULL,
  `observacoes` varchar(4000) DEFAULT NULL,
  `observacoes_chefe` varchar(4000) DEFAULT NULL,
  `data_cadastro_just` datetime DEFAULT NULL,
  `abonado` bigint(20) DEFAULT '0',
  `id_chefe` bigint(20) DEFAULT NULL,
  `data_abono` datetime DEFAULT NULL,
  `qtd_horas` varchar(10) DEFAULT NULL,
  `protocolo` varchar(100) DEFAULT NULL,
  `anexo` bigint(20) NOT NULL DEFAULT '0',
  `nome_arquivo` varchar(100) DEFAULT NULL,
  `extensao` varchar(10) DEFAULT NULL,
  `who_r` bigint(20) DEFAULT NULL,
  `ordem_justificativa` bigint(20) DEFAULT NULL,
  `decisao` mediumtext,
  `data_decisao` datetime DEFAULT NULL,
  `id_decisao` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_justificativa`),
  KEY `data_justificativa` (`data_justificativa`),
  KEY `id_justificativa` (`id_justificativa`),
  KEY `id_usr` (`id_usr`),
  KEY `id_tipo_justificativa` (`id_tipo_justificativa`)
) ENGINE=InnoDB AUTO_INCREMENT=20121 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `p_justificativa_historico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `p_justificativa_historico` (
  `id_justificativa` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_usr` bigint(20) DEFAULT NULL,
  `id_tipo_justificativa` bigint(20) DEFAULT NULL,
  `data_justificativa` datetime DEFAULT NULL,
  `observacoes` varchar(4000) DEFAULT NULL,
  `observacoes_chefe` varchar(4000) DEFAULT NULL,
  `data_cadastro_just` datetime DEFAULT NULL,
  `abonado` bigint(20) DEFAULT NULL,
  `id_chefe` bigint(20) DEFAULT NULL,
  `data_abono` datetime DEFAULT NULL,
  `qtd_horas` varchar(10) DEFAULT NULL,
  `protocolo` varchar(100) DEFAULT NULL,
  `anexo` bigint(20) NOT NULL DEFAULT '0',
  `nome_arquivo` varchar(100) DEFAULT NULL,
  `extensao` varchar(10) DEFAULT NULL,
  `who_r` bigint(20) DEFAULT NULL,
  `ordem_justificativa` bigint(20) DEFAULT NULL,
  `decisao` varchar(5000) DEFAULT NULL,
  `data_decisao` datetime DEFAULT NULL,
  `id_decisao` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_justificativa`)
) ENGINE=InnoDB AUTO_INCREMENT=19653 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `p_reenvio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `p_reenvio` (
  `id_reenvio` bigint(20) NOT NULL AUTO_INCREMENT,
  `email_envio` varchar(200) DEFAULT NULL,
  `id_gerencia` bigint(20) DEFAULT NULL,
  `data_envio` datetime DEFAULT NULL,
  `id_trabalho` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_reenvio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `p_registro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `p_registro` (
  `id_registro` bigint(20) unsigned DEFAULT NULL,
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_usr` bigint(20) DEFAULT NULL,
  `data_registro` datetime DEFAULT NULL,
  `etapa` bigint(20) DEFAULT NULL,
  `hora` varchar(5) DEFAULT NULL,
  `minutos` varchar(5) DEFAULT NULL,
  `numr_ip` varchar(150) DEFAULT NULL,
  `id_justificativa` bigint(20) DEFAULT NULL,
  `nao_registrou` bigint(20) NOT NULL DEFAULT '0',
  `origem` varchar(100) DEFAULT NULL,
  `id_registrou_chefe` bigint(20) DEFAULT NULL,
  `tsp` varchar(50) DEFAULT NULL,
  `dadosConexaoId` bigint(20) unsigned DEFAULT NULL,
  `id_grade` int(11) DEFAULT '1',
  `dados_conexao_id` varchar(191) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_usr` (`id_usr`),
  KEY `id_usr_data_registro` (`id_usr`,`data_registro`),
  KEY `data_registro` (`data_registro`),
  KEY `id_justificativa` (`id_justificativa`),
  KEY `id_registrou_chefe` (`id_registrou_chefe`),
  KEY `dadosConexaoId` (`dadosConexaoId`)
) ENGINE=InnoDB AUTO_INCREMENT=261196 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `p_superintendencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `p_superintendencia` (
  `id_superintendencia` bigint(20) NOT NULL AUTO_INCREMENT,
  `superintendencia` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_superintendencia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `p_tentativa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `p_tentativa` (
  `id_tentativa` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_usr` bigint(20) DEFAULT NULL,
  `data_tentativa` datetime DEFAULT NULL,
  `tipo_dispositivo` varchar(500) DEFAULT NULL,
  `numr_ip` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id_tentativa`)
) ENGINE=InnoDB AUTO_INCREMENT=25718 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `p_tipo_justificativa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `p_tipo_justificativa` (
  `id_tipo_justificativa` bigint(20) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(200) DEFAULT NULL,
  `cota` bigint(20) DEFAULT NULL,
  `lancamento` bigint(20) DEFAULT NULL,
  `homologa` bigint(20) DEFAULT NULL,
  `exibe` bigint(20) NOT NULL DEFAULT '1',
  `mostraRelatorio` int(10) unsigned DEFAULT '0',
  `exigeAnexo` int(10) unsigned DEFAULT '0',
  `contaComoAbonado` tinyint(3) DEFAULT '1',
  PRIMARY KEY (`id_tipo_justificativa`),
  KEY `id_tipo_justificativa` (`id_tipo_justificativa`),
  KEY `mostraRelatorio` (`mostraRelatorio`),
  KEY `contaComoAbonado` (`contaComoAbonado`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `temporario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `temporario` (
  `id_temp` bigint(20) NOT NULL AUTO_INCREMENT,
  `nome_arquivo` varchar(100) DEFAULT NULL,
  `data_foto` datetime DEFAULT NULL,
  `matricula_temp` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_temp`)
) ENGINE=InnoDB AUTO_INCREMENT=554028 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `unidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `unidades` (
  `id_unidade` bigint(20) NOT NULL AUTO_INCREMENT,
  `sigla` varchar(40) DEFAULT NULL,
  `nome` varchar(200) DEFAULT NULL,
  `titular` varchar(100) DEFAULT NULL,
  `ddd` varchar(5) DEFAULT NULL,
  `fone` varchar(15) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `tipo` bigint(20) DEFAULT NULL,
  `id_supervisor` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_unidade`),
  KEY `id_unidade` (`id_unidade`),
  KEY `id_unidade_2` (`id_unidade`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

CREATE TABLE IF NOT EXISTS `usuarios`(
 `terceirizado` int(1) not null,
 `id_usr` bigint(20) default null,
 `empresa_codigo` bigint(20) default null,
 `matricula` varchar(250) default null,
 `nome` varchar(250) default null,
 `cargo` bigint(20) default null,
 `srv_id` varchar(250) default null,
 `ativo` int(1) default null,
 `foto` varchar(250) default null,
 `id_grade` bigint(20) default null,
 `registra` int(1) default null,
 `rh` int(1) default null,
 `login` varchar(250) default null,
 `hd_user_passwd` varchar(250) default null,
 `senha` varchar(250) default null,
 `us_servico_de_autenticacao` varchar(250) default null,
 `email` varchar(250) default null,
 `id_depto` bigint(20) default null,
 `gerente` bigint(20) default null,
 `id_chefe` bigint(20) default null
)  ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `usuarios` ADD COLUMN `nascimento` VARCHAR(50) NULL AFTER `id_chefe`;
ALTER TABLE `usuarios` ADD COLUMN `data_cadastro` VARCHAR(50) NULL DEFAULT NULL AFTER `nascimento`;
ALTER TABLE `usuarios` ADD COLUMN `nome_reduzido` VARCHAR(50) NULL DEFAULT NULL AFTER `data_cadastro`;
ALTER TABLE `usuarios` CHANGE COLUMN `cargo` `cargo` VARCHAR(50) NULL DEFAULT NULL AFTER `nome`;
ALTER TABLE `usuarios` CHANGE COLUMN `terceirizado` `terceirizado` INT(1) NULL DEFAULT NULL FIRST;
ALTER TABLE `usuarios`
  CHANGE COLUMN `id_usr` `id_usr` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT AFTER `terceirizado`,
  ADD PRIMARY KEY (`id_usr`);