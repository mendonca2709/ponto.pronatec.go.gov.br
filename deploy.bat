@ECHO OFF
SET PWD=%cd%
SET PWD=%PWD:\=/%
SET PWD=%PWD:C:=/c%
SET PWD=%PWD:c:=/c%
SET TARGET=%1
IF %TARGET%.==. SET TARGET=dev

SET DOCKER_FILES_PATH=%PWD%/../docker-files/ponto

docker-compose -f docker-compose-base.yml -f docker-compose-%TARGET%.yml -p ponto stop
docker-compose -f docker-compose-base.yml -f docker-compose-%TARGET%.yml -p ponto rm
docker-compose -f docker-compose-base.yml -f docker-compose-%TARGET%.yml -p ponto up --build -d