## Sinopse

Projeto Docker Compose para a criação de ambientes que executam a versão da SED do Sistema de Ponto Eletrônico construído pela Controladoria Geral do Estado de Goiás

## Exemplo de Inicialização

Dentro da pasta raíz do projeto, execute o script `deploy.sh [dev|prod]` em distribuições linux, ou `deployt.bat [dev|prod]` em plataformas Windows

## Instalação

#### Em ambientes windodws, é necessário que o projeto reside dentro da pasta do usuário: C:\Users\Usuario\ponto.pronatec.go.gov.br

São pré-requisitos para o funcionamento do sistema:

Windows - [Windows 10 Pro com Hyper-V Habilitado](https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/quick-start/enable-hyper-v), [Docker](https://docs.docker.com/docker-for-windows/) e [Docker Toolbox](https://www.docker.com/products/docker-toolbox)

Linux - [Docker](https://docs.docker.com/engine/installation/linux/)

## Configurações

Os containers executados possuem as seguintes características:

    1. **web** utiliza a imagem: [PHP Zendserver](https://hub.docker.com/_/php-zendserver/)
    2. **db** utiliza a imagem: [MySQL](https://hub.docker.com/_/mysql/)
    3. **db-data** é um data container que utiliza a imagem: [Alpine](https://hub.docker.com/_/alpine/)

O processo de criação dos containers depende de variáveis de ambiente declaradas no arquivo [.env](./.env).
Os mesmos valores estão mapeados em **config/properties.json** no Sistema de Ponto

A execução em um ambiente Windows requer que em *Settings -> Shared Drives*, o disco *C:* esteja selecionado

## Contribuições

TI da CGE

TI da SED

NTI

## Licença

O Código Fonte do Sistema de Ponto Eletrônico foi fornecido à Secretaria de Desenvolvimento Econômico pelo departamento de T.I. da Controladoria Geral do Estado de Goiás em 2015.